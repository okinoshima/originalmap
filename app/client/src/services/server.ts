import { io } from 'socket.io-client';
import { Socket } from 'socket.io-client';
import { feathers } from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import moment from 'moment';

interface Session {
  token: string | null;
  user: User | null;
  app: App | null;
  env?: string;
}

interface User {
  _id: string | null;
}

interface App {
  _id: string | null;
}

export { Session };

let socket: Socket = null;
const NewFeathersApp = () => {
  if (typeof window === 'undefined' || !window.document) {
    return null;
  }
  socket = io(process.env.GATSBY_API_URL || window.location.origin, {
    transports: ['websocket'],
    timeout: 10000,
  });
  // コンテンツ復帰時サブスクリプションが切れる不具合の対処
  socket.on('reconnect', () => {
    window.location.reload();
  });
  const app = feathers();
  app.configure(socketio(socket));

  return app;
};

let app = NewFeathersApp();
