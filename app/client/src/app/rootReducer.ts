import { combineReducers } from '@reduxjs/toolkit';
import storeReducer from '../features/slice';
import {
  TypedUseSelectorHook,
  useSelector as rawUseSelector,
} from 'react-redux';

const rootReducer = combineReducers({
  store: storeReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export const useSelector: TypedUseSelectorHook<RootState> = rawUseSelector;

export default rootReducer;
