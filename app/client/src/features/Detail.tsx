import React, { useState } from 'react';
import Pocket from '@natade-coco/pocket-sdk';
import { useSelector } from '../app/rootReducer';
import { useDispatch } from 'react-redux';
import { changeCheckList } from './slice';
import { Spot } from './List';
import { useEffect } from 'react';
import { navigate, RouteComponentProps } from '@reach/router';

interface Props extends RouteComponentProps {
}

const Detail = (props: Props) => {
  const { checkList, spotList, selectedSpot } = useSelector(
    state => state.store
  );
  const dispatch = useDispatch();
  const [spot, setSpot] = useState<Spot>(null);
  useEffect(() => {
    setSpot(spotList[selectedSpot]);
  }, []);

  const handleClick = () => {
    navigate('/list');
  };

  const handleCheck = () => {
    dispatch(changeCheckList(selectedSpot));
  };

  let numberStyle = '';
  if (checkList[selectedSpot]) {
    numberStyle = 'has-background-danger';
  } else {
    numberStyle = 'has-background-primary';
  }

  return (
    <div>
      {spot ? (
        <div className="my-6">
          <nav className="navbar is-fixed-top">
            <div className="navbar-brand">
              <button className="navbar-item not-btn" onClick={handleClick}>
                <span className="icon is-large has-text-link">
                  <i className="fas fa-lg fa-chevron-circle-left"></i>
                </span>
              </button>
            </div>
          </nav>

          <img src={spot.image + '.png'} alt="detailImage" width="100%"></img>
          <div className="columns is-mobile mt-5">
            <div className="column is-offset-1 is-1 circle">
              <span className={numberStyle}>{selectedSpot + 1}</span>
            </div>
            <div className="column">{spot.name}</div>
          </div>
          <div className="columns is-mobile">
            <label className="column has-text-centered">
              <input
                type="checkbox"
                checked={checkList[selectedSpot] ?? false}
                onChange={handleCheck}
              ></input>
              ここに行く！
            </label>
          </div>

          <div className="columns is-mobile mt-4">
            <button
              className="column is-offset-2 is-2 not-btn"
              onClick={handleClick}
            >
              <span className="icon is-large has-text-link">
                <i className="fas fa-lg fa-chevron-circle-left"></i>
              </span>
            </button>
            <div className="column">
              <button
                className="btn"
                onClick={() => {
                  const url =
                    window.location.origin + '/' + spot.image + '.png';
                  Pocket.fileDownload(url)
                    .then(() => {
                      console.log('ダウンロード完了');
                    })
                    .catch(error => {
                      console.log('ダウンロード失敗', error);
                    });
                }}
              >
                スマホに保存
              </button>
            </div>
          </div>
        </div>
      ) : (
        'Loading'
      )}
    </div>
  );
};

export default Detail;
