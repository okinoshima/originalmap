import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from '../app/rootReducer';
import Map from '../components/Map';
import Spot from '../components/Spot';
import { changeCheckList, setCheckList, setSelectedSpot } from './slice';
import { navigate, RouteComponentProps } from '@reach/router';

interface Props extends RouteComponentProps {
}

export type LatLng = {
  lat: number;
  lng: number;
};
export type Spot = {
  text: string;
  name: string;
  image: string;
  geo: LatLng;
};

const List = (props: Props) => {
  const dispatch = useDispatch();
  const { checkList, spotList } = useSelector(state => state.store);
  const [markers, setMarkers] = useState<google.maps.Marker[]>([]);
  useEffect(() => {
    const list = spotList.map(() => false);
    if(spotList.length === 0) {
      dispatch(setCheckList(list));
    }
  }, [spotList]);

  const initializedMarker = (markers: google.maps.Marker[]) => {
    setMarkers(markers);
  };

  const handleCheck = (num: number) => {
    const isChecked = !checkList[num];
    const marker = markers[num];
    const color = isChecked ? '#ff938b' : '#3eb39f';
    const icon = marker.getIcon() as google.maps.Symbol;
    marker.setIcon({ ...icon, fillColor: color, strokeColor: color });

    dispatch(changeCheckList(num));
  };

  const handleClick = (num: number) => {
    dispatch(setSelectedSpot(num));
    navigate('/detail');
  };

  return (
    <div className="">
      <div className="map">
      <Map
        initializedMarker={initializedMarker}
        checkList={checkList}
        spotList={spotList}
        handleClick={handleClick}
      />
      </div>
      
      <div className="spotList">
      <div className="columns is-mobile mt-4">
        <div className="column is-offset-2 is-4 is-size-5 icon1">観光地</div>
        <div className="column is-offset-1 is-size-5 icon2">行く</div>
      </div>
      
      {spotList.map((spot, num) => {
        return (
          <Spot
            key={num}
            spot={spot}
            num={num}
            isChecked={checkList[num]}
            handleCheck={handleCheck}
            handleClick={handleClick}
          />         
        );
      })}

      <div className="columns is-mobile is-vcentered mt-4">
        <div className="column is-offset-2 is-3">オリジナルマップ完成</div>
        <div className="column is-2">
          <span className="has-text-primary">▶︎</span>
          <span className="has-text-warning">▶︎</span>
          <span className="has-text-danger">▶︎</span>
        </div>
        <button className="column is-3 btn mx-3">印刷</button>
      </div>
      </div>
    </div>
  );
};

export default List;
