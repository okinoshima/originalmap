import {
  createSlice,
  PayloadAction,
  createAsyncThunk,
  PayloadActionCreator
} from '@reduxjs/toolkit';
import { AppThunk } from '../app/store';
// import * as Pocket from '../services/pocket';
import * as Server from '../services/server';
import { Spot } from './List';

export interface State {
  ready: boolean;
  session?: Server.Session | null;
  isLoading: boolean;
  error: string | null;
  errorContent: { title: string; message?: string };
  checkList: boolean[];
  spotList: Spot[];
  selectedSpot?: number;
}

const initialState: State = {
  ready: false,
  session: null,
  isLoading: false,
  error: null,
  errorContent: { title: '' },
  checkList: [],
  spotList: [
    {
      text: '1',
      name: 'ローソク島',
      image: 'rosokujima',
      geo: { lat: 36.312361, lng: 133.210611 }
    },
    {
      text: '2',
      name: '岩倉の乳房杉',
      image: 'chibusugi',
      geo: { lat: 36.2585077, lng: 133.3330745 }
    },
    {
      text: '3',
      name: '壇鏡の滝',
      image: 'taki',
      geo: { lat: 36.240556, lng: 133.236111 }
    },
    {
      text: '4',
      name: '油井前の洲',
      image: 'yuinomaenosu',
      geo: { lat: 36.2410247, lng: 133.1911953 }
    },
    {
      text: '5',
      name: '水若酢神社',
      image: 'mizuwakasu',
      geo: {
        lat: 36.280217,
        lng: 133.249039
      }
    }
  ],
  selectedSpot: 0
};

function startLoading(state: State) {
  state.isLoading = true;
}

function loadingFailed(state: State, { payload }: PayloadAction<Error>) {
  state.isLoading = false;
  state.errorContent = { title: 'UNEXPECTED ERROR', message: payload.message };
  state.error = payload.message;
}

const slice = createSlice({
  name: 'root',
  initialState,

  reducers: {
    contextChanged: loadingFailed,
    getSessionStart: startLoading,
    getSessionSuccess(state, { payload }: PayloadAction<Server.Session>) {
      state.session = payload;
      state.isLoading = false;
      state.error = null;
    },
    getSessionFailed: loadingFailed,
    setCheckList(state, { payload }: PayloadAction<boolean[]>) {
      state.checkList = payload;
    },
    changeCheckList(state, { payload }: PayloadAction<number>) {
      const newCheckList = state.checkList.slice();
      newCheckList[payload] = !state.checkList[payload];
      state.checkList = newCheckList;
    },
    setSelectedSpot(state, { payload }: PayloadAction<number>) {
      state.selectedSpot = payload;
    }
  }
});

export const {
  contextChanged,
  getSessionStart,
  getSessionSuccess,
  getSessionFailed,
  changeCheckList,
  setCheckList,
  setSelectedSpot
} = slice.actions;

export default slice.reducer;
