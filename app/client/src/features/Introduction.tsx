import React from 'react';
import { navigate, RouteComponentProps } from '@reach/router';

interface Props extends RouteComponentProps {
}

const Introduction = (props: Props) => {
    const handleClick = () => {
        navigate('/list');
    }

    return(
        <div>
            <div className="has-text-centered">
                <div className="title">隠岐を知る
                    <span className="br">あなただけの旅の地図</span>
                </div>
            </div>
            <div className="subtitle">オリジナルマップをつくろう！</div>
            <div className="margin">
                <div>オリジナルマップに書かれる目的地はあなたが選んだスポットだけ。
                    <span className="br">あなた専用の地図をつくって、隠岐の島を楽しもう！</span>
                </div>
            </div>
            <div className="margin2">3ステップでできあがり</div>
            <div className="step">01</div>
            <div className="explanation">観光スポットの詳しい情報を見る</div>
            <div className="step">02</div>
            <div className="explanation">目的地を決めてチェックをつける</div>
            <div className="step">03</div>
            <div className="explanation">完成したオリジナルマップを印刷</div>
            <div className="has-text-centered">
                <button className="btn" onClick={handleClick}>START</button>
            </div>
        </div>
    )
}

export default Introduction;