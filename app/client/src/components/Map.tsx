import GoogleMapReact from 'google-map-react';
import React, { Props } from 'react';
import { ClassType } from 'react';
import { Spot } from '../features/List';
import MarkerClusterer from '@googlemaps/markerclustererplus'

type MapProps = {
  spotList: Spot[];
  checkList: boolean[];
  initializedMarker: (markers: google.maps.Marker[]) => void;
  handleClick: (num: number) => void;
};

const Map = (props: MapProps) => {
  const defaultCenter = {
    lat: 36.2543677,
    lng: 133.273053
  };

  const mapStyle = {
    height: '50vh',
    width: '100%'
  };

  const markerPosition = ({
    map,
    maps
  }: {
    map: google.maps.Map;
    maps: any;
  }) => {

    new maps.Marker({
      position: {
        lat: 36.2034996,
        lng: 133.3351445,
      },
      icon: {
        fillColor: '#0000ff',
        fillOpacity: 0.8,
        strokeColor: '#0000ff',
        scale: 18,
        path: maps.SymbolPath.CIRCLE,
        strokeWeight: 1
      },
      label: {
        color: 'white',
        fontFamily: 'sans-serif',
        fontSize: '11.5px',
        fontWeight: '400',
        text: '西郷港'
      },
      map,
    })

    const markers: google.maps.Marker[] = props.spotList.map((spot, num) => {
      const color = props.checkList[num] ? '#ff938b' : '#3eb39f';
      const marker: google.maps.Marker = new maps.Marker({
        position: {
          lat: spot.geo.lat,
          lng: spot.geo.lng
        },
        icon: {
          fillColor: color,
          fillOpacity: 0.5,
          strokeColor: color,
          scale: 16,
          path: maps.SymbolPath.CIRCLE,
          strokeWeight: 1
        },
        label: {
          color: 'white',
          fontFamily: 'sans-serif',
          fontSize: '15px',
          fontWeight: '350',
          text: spot.text
        },
        map,
      });
      marker.addListener('click', (event: any) => clickTransition(num))
      return marker;     
    });
    props.initializedMarker(markers);
    
    const imagePath = "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m"
    const markerClusterer = new MarkerClusterer(map, markers, {imagePath: imagePath});

    const clustererStyles = markerClusterer.getStyles();
    for (let i=0; i<clustererStyles.length; i++) {
      clustererStyles[i].textColor = "white";
      clustererStyles[i].textSize = 13;
    }
  };

  function clickTransition(num: number){
    props.handleClick(num);
  }

  return (
    <div style={mapStyle}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: process.env.GOOGLE_MAP_API_KEY }}
        defaultCenter={defaultCenter}
        defaultZoom={10.9}
        onGoogleApiLoaded={markerPosition}
      ></GoogleMapReact>
    </div>
  );
};
export default Map;