import * as React from 'react';
import { Spot } from '../features/List';

type SpotProps = {
  spot: Spot;
  num: number;
  isChecked: boolean;
  handleCheck: (num: number) => void;
  handleClick: (num: number) => void;
};

const SpotComponent = (props: SpotProps) => {
  const { spot, num, isChecked, handleCheck, handleClick } = props;

  let numberStyle = '';
  if (isChecked) {
    numberStyle = 'has-background-danger';
  } else {
    numberStyle = 'has-background-primary';
  }

  return (
    <div className="columns is-mobile">
      <div className="column is-offset-1 is-1 circle">
        <span className={numberStyle}>{num + 1}</span>
      </div>
      <button
        className="column is-7 spot-link"
        onClick={() => handleClick(num)}
      >
        {spot.name}
      </button>
      <div className="column">
        <input
          type="checkbox"
          checked={isChecked ?? false}
          onChange={() => handleCheck(num)}
        ></input>
      </div>
    </div>
  );
};

export default SpotComponent;
