import * as React from 'react';
import './styles.scss';

import List from '../features/List';
import Detail from '../features/Detail';
import Router from '../components/Router';
import Introduction from '../features/Introduction'

const IndexPage = () => {
  return (
    <>
      <link
        href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
        rel="stylesheet"
      ></link>
      <Router>
        <Introduction path="/" />
        <List path="/list" />
        <Detail path="/detail" />
      </Router>
    </>
  );
};

export default IndexPage;
